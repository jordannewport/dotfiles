set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 8

exec_always --no-startup-id dunst
# exec_always --no-startup-id hsetroot -cover ~/.config/i3/chandra.jpg -brightness -.3
exec_always --no-startup-id hsetroot -cover ~/Pictures/cyberpunk-city-concept-art-4k-we.jpg -gamma 0.5
exec_always --no-startup-id picom --config ~/.config/picom/picom.conf -b

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn’t scale on retina/hidpi displays.

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
# bindsym $mod+Return exec urxvt -fg white -fade 15
bindsym $mod+Return exec kitty

# kill focused window
bindsym $mod+Shift+q kill
bindsym $mod+x kill

# start dmenu (a program launcher)
bindsym $mod+d exec dmenu_run
bindsym $mod+space exec dmenu_run
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
# bindsym $mod+d exec --no-startup-id i3-dmenu-desktop

# change focus
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right
bindsym $mod+h focus left

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right
bindsym $mod+Shift+h move left

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+semicolon split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+u focus mode_toggle

# focus the parent container
# bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-msg exit"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 5 px or 5 ppt
        bindsym j resize grow height 5 px or 5 ppt
        bindsym k resize shrink height 5 px or 5 ppt
        bindsym l resize grow width 5 px or 5 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 5 px or 5 ppt
        bindsym Down resize grow height 5 px or 5 ppt
        bindsym Up resize shrink height 5 px or 5 ppt
        bindsym Right resize grow width 5 px or 5 ppt

        # back to normal: Enter or Escape or $mod+z
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+z mode "default"
}
bindsym $mod+z mode "resize"

# control cmus using cmus-remote
mode "cmus-remote" {
    # play
    bindsym Return exec cmus-remote -p
    # toggle pause
    bindsym space exec cmus-remote -u
    # next/prev
    bindsym XF86AudioNext exec cmus-remote -n
    bindsym XF86AudioPrev exec cmus-remote -r
    # toggle continue/shuffle/repeat
    bindsym c exec cmus-remote -C "toggle continue"
    bindsym r exec cmus-remote -R
    bindsym s exec cmus-remote -S
    # seek in track
    bindsym h exec cmus-remote -k -5
    bindsym l exec cmus-remote -k +5
    bindsym shift+h exec cmus-remote -k -1m
    bindsym shift+l exec cmus-remote -k +1m
    # exit
    bindsym Escape mode "default"
    bindsym $mod+c mode "default"
    bindsym $mod+m mode "default"
}
bindsym $mod+c mode "cmus-remote"
bindsym $mod+m mode "cmus-remote"

# control the mouse using the keyboard using xdotool
mode "mouse" {
    set $distance 50
    set $small_distance 5
    # move the mouse. Optimized to feel good for 1080p screen
    bindsym h exec xdotool mousemove_relative -- -$distance 0
    bindsym j exec xdotool mousemove_relative -- 0 $distance
    bindsym k exec xdotool mousemove_relative -- 0 -$distance
    bindsym l exec xdotool mousemove_relative -- $distance 0
    # move the mouse less, for precise actions
    bindsym shift+h exec xdotool mousemove_relative -- -$small_distance 0
    bindsym shift+j exec xdotool mousemove_relative -- 0 $small_distance
    bindsym shift+k exec xdotool mousemove_relative -- 0 -$small_distance
    bindsym shift+l exec xdotool mousemove_relative -- $small_distance 0
    # click
    bindsym u exec xdotool click 1 # left
    bindsym y exec xdotool mousedown 1 # depress mouse. for click and drag
    bindsym space exec xdotool mouseup 1 # release mouse. for ending click and drag
    bindsym i exec xdotool click 3 # right
    bindsym m exec xdotool click 2 # middle
    # scroll
    bindsym p exec xdotool click 4 # up
    bindsym semicolon exec xdotool click 5 # down
    # exit
    bindsym $mod+i mode "default"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+i mode "mouse"

hide_edge_borders both

for_window [class="SFML Window"] floating enable
for_window [class="coreto"] floating enable

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        status_command i3status
}

bindsym XF86AudioLowerVolume    exec $psst pactl set-sink-volume @DEFAULT_SINK@ -5% && pactl set-sink-mute @DEFAULT_SINK@ 0 $update
bindsym XF86AudioRaiseVolume    exec $psst pactl set-sink-volume @DEFAULT_SINK@ +5% && pactl set-sink-mute @DEFAULT_SINK@ 0 $update
bindsym XF86AudioMute           exec $psst pactl set-sink-mute @DEFAULT_SINK@ toggle $update
bindsym XF86MonBrightnessDown   exec xbacklight -dec 10
bindsym XF86MonBrightnessUp     exec xbacklight -inc 10
bindsym $mod+XF86MonBrightnessDown     exec xbacklight -set 1
# Print screen 
bindsym Print exec maim ~/Pictures/Screenshots/Screenshot$(date +%Y-%m-%d-%T).png

# Print screen selected window
bindsym --release $mod+n exec "maim -us ~/Pictures/Screenshots/Screenshot$(date +%Y-%m-%d-%T).png"

#screen locker
bindsym $mod+o exec i3lock -e --color 000000
# make transparency not happen because there's no background image. Restart i3 to undo
bindsym $mod+Shift+t exec hsetroot -solid black

# fix Firefox control q
bindsym Control+q exec ~/.local/bin/noctrlq.sh
