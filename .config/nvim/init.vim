" In large part copied from https://gitlab.com/sumner/dotfiles/
" Plug------------------------------------------------------------------------
call plug#begin()
Plug 'kien/rainbow_parentheses.vim'
Plug 'gioele/vim-autoswap' " deal with some of those swapfile messages
Plug 'terryma/vim-multiple-cursors' " Allow multiple cursors TODO: learn to use this
Plug 'tpope/vim-commentary' " Easy commenting
Plug 'tpope/vim-surround' " Easy surrounding delimiters
Plug 'tpope/vim-repeat' " . works on more plugins
Plug 'jiangmiao/auto-pairs' " Automatically close delimiters
Plug 'mboughaba/i3config.vim' " better i3 syntax highlighting
" Language client
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'mbbill/undotree' " show undo history as tree
Plug 'rhysd/vim-crystal' " Crystal support
call plug#end()

" Colors-----------------------------------------------------------------------
" if $TERM == 'xterm-256color' && !has('gui_running')
"     set termguicolors
" endif
" column guides should be grey not red
highlight ColorColumn ctermbg=234
" parentheses (etc.) match should not make the delimiter itself unreadable, so
" it's blue now
highlight MatchParen ctermbg=020
" highlight the current cursor line in grey, but don't underline it
set cursorline 
highlight clear CursorLine
highlight CursorLine ctermbg=234
" sign column should be black
highlight SignColumn ctermbg=0

" UI--------------------------------------------------------------------------
set colorcolumn=80,100 " Column guides
set mouse=a " Enable mouse scrolling
set number " Show the current line number
set signcolumn=yes " Always show the sign column for git gutter/ale linters
set title " override terminal title
set ignorecase      "Ignore case in all searches...
set smartcase       " ...unless uppercase letters used
" search as I type, highlight results
set incsearch
set showmatch
set hlsearch
set laststatus=0 " no idea if I actually need this or not
" hitting esc twice cancels search highlighting
nnoremap <silent> <Esc><Esc> <Esc>:nohlsearch<CR><Esc>
" set list listchars=tab:»·,trail:·  " show tiny dot on space, arrows on tab char. Could be space not trail for all spaces
set list listchars=tab:»\ ,trail:·  " show tiny dot on space, arrows on tab char. Could be space not trail for all spaces

" Plugin configs---------------------------------------------------------------
" auto pairs
let g:AutoPairs = {'(':')', '[':']', '{':'}', '"':'"', "`":"`", '```':'```', '"""':'"""', "'''":"'''"} " take out auto matching '' and \"\" because f'' is a pain

" coc
set hidden
set cmdheight=2
set updatetime=300
set shortmess+=c

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use <tab> for select selections ranges, needs server support, like: coc-tsserver, coc-python
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <S-TAB> <Plug>(coc-range-select-backword)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

" Supertab
let g:SuperTabDefaultCompletionType = '<c-n>' " Tab goes down list instead of up

" ncm2
" autocmd BufEnter * call ncm2#enable_for_buffer() " enable ncm2 for all buffers
set completeopt=menu,menuone,preview,noselect,noinsert

" Rainbow parentheses
let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['yellow',     'yellow'],
    \ ['red',         'firebrick3'],
    \ ]
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" I think I need this to make polyglot work?
syntax on

" UndoTree
nnoremap gu :UndotreeToggle<cr> " turn undotree on/off
let g:undotree_WindowLayout = 2 " set layout: diff on bottom
let g:undotree_ShortIndicators = 1 " use short names for time e.g. s for seconds
let g:undotree_DiffAutoOpen = 0 " don't auto-open diff panel
let g:undotree_SetFocusWhenToggle = 1 " give undotree focus when opened (q can still close)

" Editing---------------------------------------------------------------------
set shiftwidth=4 " 4 space tab
set tabstop=4 " 4 space tab
set expandtab " tabs replaced with spaces
autocmd FileType * syntax spell toplevel " Make spelling a top level syntax element
" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" Save on focus lost
au FocusLost * silent! :wa
" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif
" Remember info about open buffers on close
set viminfo^=%
" Undo and swap
set undofile " Use an undo file
set undodir=~/.cache/nvim/undo// " undo files
set directory=~/.cache/nvim/swap// " swap files
" remap control y and control e (scroll lines without moving cursor) to better keys
" FIXME: this moves one char forward for some reason and appending h is a hacky way to fix it
nnoremap <C-K> <C-Y> h
nnoremap <C-J> <C-E> h
" execute the q macro
nnoremap Q @q
set inccommand=nosplit " show in real time what changes ex command will make
" give windows a mildly less terrible keybinding
nnoremap W <C-w>
" set tildeop " can use ~ to change case with a motion, not just current v-selection
" make ~l advance a character
" nnoremap ~l ~ll
" turn on modelines
set modeline
set modelines=5

" Filetype specific configurations--------------------------------------------
"
" Automatically break lines at 80 characters on TeX/LaTeX, Markdown, and text files
" Also enable spell check on Markdown and text files.
" Not TeX because tex commands are not valid words and spell gets pissy
autocmd BufNewFile,BufRead *.tex,*.md,*.txt,*.rst setlocal tw=80
autocmd BufNewFile,BufRead *.tex,*.md,*.txt,*.rst setlocal linebreak breakindent
autocmd BufNewFile,BufRead *.md,*.txt,*.rst setlocal spell spelllang=en_us
autocmd BufNewFile,BufRead *.tex,*.md,*.txt,*.rst highlight Over100Length none

" Make i3 config syntax highlighting via i3config.vim plugin work
autocmd BufNewFile,BufRead $HOME/.config/i3/config setlocal filetype=i3config

" Make sure Scala files work au BufRead,BufNewFile *.sbt set filetype=scala
" Make sure Crystal files work
au BufRead,BufNewFile *.cr set filetype=crystal
let g:crystal_define_mappings=0

" Automatically break lines at 80 characters when writing emails in mutt
" Enable spell check for emails in mutt and quotes file
autocmd BufRead /tmp/mutt-*,$HOME/.mutt/quotes setlocal tw=72
autocmd BufRead /tmp/mutt-*,$HOME/.mutt/quotes setlocal spell spelllang=en_us
autocmd BufRead /tmp/mutt-*,$HOME/.mutt/quotes setlocal colorcolumn=72,80
" autocmd BufRead /tmp/mutt-*,$HOME/.mutt/quotes match Over100Length /\%73v.\+/

" make RST and emacs orgmode files automatically unfold
autocmd FileType rst,org setlocal nofoldenable
autocmd FileType scala set shiftwidth=4 " this sets itself to 2 for no good reason

" vim-commentary for crystal
autocmd FileType crystal setlocal commentstring=#\ %s

autocmd FileType c,go setlocal shiftwidth=8
autocmd FileType c,go setlocal tabstop=8
autocmd FileType c,go setlocal noexpandtab
