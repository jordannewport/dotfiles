setopt NO_HUP # these two allow ampersand to spin off jobs while I reuse or close the terminal
setopt NO_CHECK_JOBS
setopt no_notify # don't tell me about the status of a terminated background job
setopt extendedglob # good globbing for zsh. ** etc
setopt globstarshort
# history settings
setopt INC_APPEND_HISTORY # --immediately add new history
setopt histignoredups # only store one of a series of identical commands
setopt histignorespace # don't save lines starting with whitespace to history

setopt nobeep # no terminal bell
setopt autocd # cd using just directory name. Like suffix alias but for directory
setopt autopushd # put name of directory cd'd to on directory stack
setopt pushd_ignore_dups # don't put the same dir on directory stack several times in a row
setopt correct # autocorrect for the shell

setopt numeric_glob_sort
setopt interactive_comments
