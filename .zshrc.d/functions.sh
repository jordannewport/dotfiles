#things to do after cd
function chpwd() {
    ls
}

#make a directory and switch to it
mkcd() { mkdir "$@" && cd "$@" && echo "don't ls" }

# you ever want to just use find to find a goddamn file matching a regex? Me too.
# given a full regex
findr() {find . -regex "$@"}
# given just the payload (you know, like ls | grep $@ but saner)
finds() {find . -regex ".*$@.*"}

#obvious
function __zkey_prepend_sudo {
    if [[ $BUFFER != "sudo "*  ]]; then
        BUFFER="sudo $BUFFER"
        CURSOR+=5
    else
        BUFFER="${BUFFER:5}"
    fi
}
zle -N prepend-sudo __zkey_prepend_sudo

function __zkey_prepend_vim {
    if [[ $BUFFER != "nvim "*  ]]; then
        BUFFER="nvim $BUFFER"
        CURSOR+=5
    else
        BUFFER="${BUFFER:5}"
    fi
}
zle -N prepend-vim __zkey_prepend_vim

function __zkey_prepend_man {
    if [[ $BUFFER != "man "*  ]]; then
        BUFFER="man $BUFFER"
        CURSOR+=4
    else
        BUFFER="${BUFFER:4}"
    fi
}
zle -N prepend-man __zkey_prepend_man

#"delete" things to /tmp recycle bin
function del() {
    mv $* /tmp
}

# Magic dot key--from github.com/jackrosenthal/dotfiles
# if there are already 2 dots then typing dot puts /..
function __zkey_dot_handler {
    [[ $LBUFFER == *..  ]] && LBUFFER+="/.." || LBUFFER+="."
}
zle -N magic-dot __zkey_dot_handler

sm() {sudo systemctl "$1" mvpn}
