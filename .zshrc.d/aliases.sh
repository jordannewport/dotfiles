alias ls='ls --color=auto' # for ls
alias l='ls'
alias ll='ls -l'
alias lh='ls -lh'
alias la='ls -A'
alias lla='ls -lA'
# alias msword='WINEPREFIX="$HOME/prefix32" WINEARCH=win32 wine /home/jordan/prefix32/drive_c/Program\ Files/Microsoft\ Office/root/Office16/WINWORD.exe' # microsoft office via wine
# alias mspwp='WINEPREFIX="$HOME/prefix32" WINEARCH=win32 wine /home/jordan/prefix32/drive_c/Program\ Files/Microsoft\ Office/root/Office16/POWERPNT.exe'
# alias msexcel='WINEPREFIX="$HOME/prefix32" WINEARCH=win32 wine /home/jordan/prefix32/drive_c/Program\ Files/Microsoft\ Office/root/Office16/EXCEL.exe'
alias gpp="g++"
alias gppsfml="g++ -c main.cpp && g++ main.o -o sfml-app -lsfml-graphics -lsfml-window -lsfml-system"
alias connect='intel-virtual-output && xrandr --output eDP1 --mode 1920x1080 --output VIRTUAL1 --mode VIRTUAL1.447-1920x1080 --left-of eDP1'
alias new_connect="intel-virtual-output && xrandr --output eDP1 --mode 1920x1080 --output VIRTUAL1 --mode VIRTUAL1.447-2560x1440 -r 143.91 --left-of eDP1"
alias monitor_primary="xrandr --output VIRTUAL1 --primary"
alias kivo="pkill -f intel-virtual-output"
alias xkcd="echo 'import antigravity' | python"
alias update="sudo pacman -Syu --ignore linux,linux-firmware,nvidia,bbswitch,nvidia-utils,lib32-nvidia-utils, linux-headers"
alias yayupdate="yay -Syu --ignore linux,linux-firmware,nvidia,bbswitch,nvidia-utils,lib32-nvidia-utils"
alias ":q"="exit"
alias ":wq"="exit"
alias q="exit"
# file redirect a pdf into these two. print a pdf to ALAMODE printer
alias alpr="ssh jordannewport@isengard lpr -P bb136-printer -o coallate=true"
alias alprd="ssh jordannewport@isengard lpr -P bb136-printer -o coallate=true -o Duplex=DuplexNoTumble"
alias u="urxvt -fg white -fade 15 &"
alias n="nvim"
alias v="nvim"
alias zathura="zathura --fork"
alias z="zathura"
alias antioffice='libreoffice --headless --convert-to pdf'
alias d="del"
alias c="cd"
alias map="setxkbmap us && xmodmap ~/.Xmodmap"
alias map2="setxkbmap 3ls && xmodmap ~/.Xmodmap"
alias map3l="setxkbmap 3l"
alias xctu="~/Desktop/XCTU.desktop"
alias info="info --vi-keys"
alias grep="grep --color=auto"
alias processing="~/Downloads/processing/processing-3.5.3/processing"
alias gdb="gdb -q"
alias kb="xmodmap ~/.Xmodmap" # needed because hotplugged keyboards don't automatically get xmodmapped
alias dif="git diff --no-index"
alias flake8="flake8 --ignore E501,E261,E302,W503,W504" # in the long term I would like to not do it this way, but there is no documentation for configuring flake8
alias m="mutt"
alias pm="sudo pacman"
alias desktop-monitors="xrandr --output DisplayPort-1 --primary --mode 2560x1440 -r 143.91 --output DisplayPort-2 --mode 1920x1080 --right-of DisplayPort-1"
alias left-monitor="xrandr --output DisplayPort-1 --primary --mode 2560x1440 -r 143.91 --output DisplayPort-2 --off"
alias rn="sudo systemctl restart NetworkManager"
alias feh="feh -."
alias man="MANWIDTH=80 man --nj --nh"
alias rsbcl="rlwrap sbcl"
alias kimg="kitty +kitten icat"
alias mpva="mpv --no-video"
alias camera="ffmpeg -f v4l2 -video_size 640x480 -i /dev/video0 -c:v libx264 -preset ultrafast webcam.mp4"

# aliases for git
alias g="git"
alias ga="git add"
alias gau="git add --update"
alias gc="git commit"
alias gco="git checkout"
alias gd="git diff -b" # ignore whitespace changes, like DOS vs Linux endings or indentation changes
alias gdc="git diff --cached"
alias gfetch="git fetch"
alias gl="git log --pretty=format:'%C(auto)%h %ad %C(green)%s%Creset %C(auto)%d [%an (%G? %GK)]' --graph --date=format:'%Y-%m-%d %H:%M' --all"
alias gpl="git pull"
alias gpll="git pull"
alias gpsh="git push"
alias grh="git reset HEAD"
alias grhh="git reset --hard HEAD"
alias gs="git status"
alias gst="git stash"
alias gb="git branch"
alias gdcs="git diff --compact-summary"

# global aliases
alias -g mvpn="openvpn-client@client.ovpn.service"
alias -g hwl="~memo/todo/homework"
alias -g userchrome="~/.mozilla/firefox/3sxxcgex.default/chrome/userChrome.css"
# mainly for mbsync
alias -g jw="jordanwargames"
alias -g mi="mines"
alias -g mg="migadu"

# emacs bullshit
alias emacsd="emacs --daemon"
alias e=emacsclient
alias kill-emacs='e -e "(kill-emacs)"'
