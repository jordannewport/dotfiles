bindkey -v #vim mode 
bindkey -M vicmd q push-line #q to enter a command, then go back to the old one
bindkey -M vicmd "K" prepend-sudo #obvious
bindkey -M vicmd "m" prepend-man
bindkey -M vicmd "v" prepend-vim
bindkey -M viins '.' magic-dot
bindkey "^R" history-incremental-search-backward #reverse i-search like most shells/repls have
# up arrow search based on what is already typed
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search # Up
bindkey "^[[B" down-line-or-beginning-search # Down
bindkey -M vicmd "k" up-line-or-beginning-search
bindkey -M vicmd "j" down-line-or-beginning-search

bindkey "^?" backward-delete-char # fix a thing zsh viins mode does
bindkey "^H" backward-kill-word # fix a thing zsh viins mode does

bindkey '\e[3~' delete-char
bindkey '\e[3;5~' kill-word
