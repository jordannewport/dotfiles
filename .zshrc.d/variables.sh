# variables
export PAGER=less
export WORKON_HOME=/home/jordan/.virtualenvs
export EDITOR=nvim
export VISUAL=$EDITOR
export XDG_CONFIG_HOME=/home/jordan/.config
export XDG_CONFIG_DIR=$XDG_CONFIG_HOME
export WINIT_HIGH_DPI_FACTOR=1
export IMAGEVIEWER=feh
export OFFICE=libreoffice
export PDFVIEWER="zathura --fork"
export VIDEOVIEWER=mpv
export WINE=wine
export GOPATH=~/go

# path
export PATH="$HOME/.local/bin:$PATH"
# export PATH=$PATH:~/.gem/ruby/2.6.0/bin # for ruby apps. May remove later
# export PATH=$PATH:~/.yarn/bin # for js packages installed with yarn

# hist config
HISTFILE=~/.histfile
HISTSIZE=50000
SAVEHIST=50000

# zsh-autosuggestions plugin config
export ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=8'
