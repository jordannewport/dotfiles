autoload -Uz promptinit && promptinit
setopt prompt_subst
autoload colors zsh/terminfo
colors
function preexec() {
    cmd_start=$SECONDS
}

# left prompt
HOSTNAME=$(hostname)
export PROMPT="%{$fg[cyan]%}${USERNAME:0:1}@${HOSTNAME:0:1} %3~ %# %{$fg[white]%}"

function __rprompt {

    # Local variables for color
    local WHITE="%F{white}"
    local YELLOW="%F{yellow}"
    local GREEN="%F{green}"
    local RED="%F{red}"
    local RESET="%f"

    # Capture result of last command. This is displayed at the end of the
    # prompt.
    if [[ $? -eq 0 ]]; then
        RESULT="[%F{green}%?%f]"
    else
        RESULT="[%F{red}%B%?%b%f]"
    fi

    # If we are in a Git repo, show the branch and the status, as well as
    # whether we need to push/pull.
    git rev-parse --git-dir >& /dev/null  # Detect git repo
    if [[ $? == 0 ]]; then
        echo -n "["

        if [[ `git ls-files -u >& /dev/null` == '' ]]; then
            # If files have been modified, dirty.
            [[ `git diff` != '' ||
                `git ls-files --others --exclude-standard` != '' ||
                `git diff --staged` != '' ]] && echo -n $YELLOW || echo -n $GREEN

            # If files have been staged for commit, add a "+"
            [[ `git diff --staged` != '' ]] && echo -n "+"
        else
            # Untracked files
            echo -n $RED
        fi
        # Show the branch name.
        echo -n `git branch | command grep '* ' | sed 's/..//' || echo 'No Branch'`

        # Determine if need to pull or not
        UPSTREAM=${1:-'@{u}'}
        LOCAL=$(git rev-parse @ 2&>/dev/null)
        REMOTE=$(git rev-parse "$UPSTREAM" 2&>/dev/null)
        BASE=$(git merge-base @ "$UPSTREAM" 2&>/dev/null)

        if [[ $LOCAL != $REMOTE && $REMOTE != "" ]]; then
            echo -n " ("
            if [ $LOCAL = $BASE ]; then
                echo -n "v" # Need to pull
            elif [ $REMOTE = $BASE ]; then
                echo -n "^" # Need to push
            else
                echo -n "^v" # Diverged
            fi
            echo -n ")"
        fi
        echo -n $RESET
        echo -n "]"
    fi

    # Echo the result of the previous call.
    echo -n $RESULT
}
export RPS1='$(__rprompt) '
