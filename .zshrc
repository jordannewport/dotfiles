#~/.zshrc
# source regular files
for file in ~/.zshrc.d/*; do
    source $file
done

# plugins
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
# source /usr/share/zsh/plugins/zsh-you-should-use/you-should-use.plugin.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh # note to self: must be last

#sometimes I start typing early and I hate seeing it
eval clear
