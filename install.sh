#!/bin/sh
ln .clang-format ~
ln .gitconfig ~
ln .gitignore_global ~

mkdir ~/.ssh
ln .ssh/config ~/.ssh

echo 'figure out your Firefox userchrome on your own'

ln .xinitrc ~
ln .Xmodmap ~
ln .Xresources ~
ln .zshrc ~
ln -s .zshrc.d ~/.zshrc.d
mkdir ~/.config
# ignoring alacritty since I don't actually use it anymore
mkdir ~/.config/cava
mkdir ~/.config/cmus
mkdir ~/.config/dunst
mkdir ~/.config/i3
mkdir ~/.config/i3status
mkdir ~/.config/nvim
ln .config/cava/config ~/.config/cava
ln .config/cmus/rc ~/.config/cmus
ln .config/dunst/dunstrc ~/.config/dunst
ln .config/i3/config ~/.config/i3
ln .config/nvim/init.vim ~/.config/nvim
ln .config/nvim/coc-settings.json ~/.config/nvim
